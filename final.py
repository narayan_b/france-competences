import pandas as pd
import sqlite3

xlsx = pd.ExcelFile("Referentiel-des-NPEC-15.10.2023_vMAJ-29.01.2024.xlsx")
df = pd.read_excel(xlsx, "Onglet 3 - référentiel NPEC", skiprows=3)

df.columns=['code_RNCP', 'l_formation', 'certif', 'diplome', 'code_CPNE', 'CPNE', 'amount', 'statut', 'date']

df['date'] = pd.to_datetime(df['date']).dt.strftime('%Y-%m-%d')

idcc_df = pd.read_excel(xlsx, "Onglet 4 - CPNE-IDCC", skiprows=2)
idcc_df.columns=['code_CPNE', 'CPNE', 'IDCC']

#-------------------------------------------------------
#            Convert objects into lists                |
#-------------------------------------------------------


idcc_values = idcc_df[['IDCC', 'code_CPNE']].values.tolist()
cpne_values = df[['code_CPNE', 'CPNE', 'amount']].values.tolist()
certif_values = df['certif'].tolist()
npec_values = df[['statut', 'date']].values.tolist()


valid_npec_values = df[df['statut'].isin(['A', 'CPNU', 'R'])][['statut', 'date']].values.tolist()

#-------------------------------------------------------
#            Create Table in SQL Lite                  |
#-------------------------------------------------------

con = sqlite3.connect("france_competence.db")
cur = con.cursor()

cur.execute("""CREATE TABLE IF NOT EXISTS convention(
            idcc INTEGER PRIMARY KEY,
            code_cpne INTEGER,
            FOREIGN KEY (code_cpne) REFERENCES cpne(code_cpne)
)
""")

cur.execute("""
CREATE TABLE IF NOT EXISTS cpne(
            code_cpne INTEGER PRIMARY KEY,
            name_cpne VARCHAR(255),
            amount INTEGER
)
""")

cur.execute("""
CREATE TABLE IF NOT EXISTS certificateur(
            id_certif INTEGER PRIMARY KEY,
            name_certif VARCHAR(255)
)
""")

cur.execute("""
CREATE TABLE IF NOT EXISTS formation(
            code_rncp VARCHAR(255) PRIMARY KEY,
            libelle_de_formation VARCHAR(255),
            libelle_de_diplome VARCHAR(255),
            id_certif INTEGER,
            FOREIGN KEY (id_certif) REFERENCES certificateur(id_certif)
)
""")

cur.execute("""
CREATE TABLE IF NOT EXISTS npec(
            id_npec INTEGER PRIMARY KEY,
            status TEXT CHECK (status IN ('A', 'CPNU', 'R')),
            date DATE
)
""")

cur.execute("""
CREATE TABLE IF NOT EXISTS connect(
            id_npec INTEGER,
            code_rncp INTEGER,
            code_cpne INTEGER,
            FOREIGN KEY (id_npec) REFERENCES npec(id_npec),
            FOREIGN KEY (code_rncp) REFERENCES formation(code_rncp),
            FOREIGN KEY (code_cpne) REFERENCES cpne(code_cpne)
)
""")

cur.executemany("INSERT INTO convention(idcc, code_cpne) VALUES(?, ?)", idcc_values)
cur.executemany("INSERT OR IGNORE INTO cpne(code_cpne, name_cpne, amount) VALUES(?, ?, ?)", cpne_values)
cur.executemany("INSERT INTO certificateur(name_certif) VALUES(?)", [(certif,) for certif in certif_values])
cur.executemany("INSERT INTO npec(status, date) VALUES (?, ?)", valid_npec_values)

# cur.execute("""INSERT INTO convention(idcc, code_cpne) VALUES(?, ?)""", (idcc_df['IDCC'], idcc_df['code_CPNE']))
# cur.execute("""INSERT INTO cpne(code_cpne, name_cpne, amount) VALUES(?, ?, ?)""", (df['code_CPNE'], df['CPNE'], df['amount']))
# cur.execute("""INSERT INTO certificateur(name_certif) VALUES(?)""", (df['certif']))
# # cur.execute("""INSERT INTO formation(code_rncp, libelle_de_formation, libelle_de_diplome, id_certif) VALUES(?, ?, ?, ?)""", (df['code_RNCP'], df['l_formation'], df['diplome'], ('select id_certif from certificateur')))
# cur.execute("""INSERT INTO npec(status, date) VALUES(?, ?, ?)""", (df['statut'], df['date']))
# # cur.execute("""INSERT INTO connect(id_npec, code_rncp, code_cpne) VALUES(?, ?, ?)""", (df['statut'], df['date']))

cur.executemany("SELECT * FROM npec")


con.commit()
con.close()


